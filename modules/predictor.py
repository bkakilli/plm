import torch
from ..lstm import RNN

class Predictor:

    cases = [
        'Healthy',
        'Prop 1 Broken',
        'Prop 2 Broken'
    ]

    def __init__(self):

        self.rnn = None
        self.input_size = 3
        self.num_cases = len(Predictor.cases)
        self.data_len = 88

    def load(self):
        self.rnn = RNN(self.input_size, self.num_cases, data_len=self.data_len)
        # Load pretrained weights
        raise NotImplementedError()

    def predict(self, vibration_data):
        """ Predict time-series vibration data.
        Input:
            (np.ndarray) vibration_data: Nx3 timeseries data
        Returns:
            (str) Predicted case
        """
        vibration_data_torch = torch.from_numpy([vibration_data])

        logits = self.rnn(vibration_data_torch.float())
        predicted_class = torch.max(logits, 1)[1].numpy()

        return Predictor.cases[predicted_class]

