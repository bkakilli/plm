import requests
import numpy as np

class RemoteDatabase:
    def __init__(self):
        self.connected = False
        raise NotImplementedError()

    def connect(self, inet):
        self.url = inet
        if self.check_connection():
            self.connected = True
        else:
            raise ConnectionError()

    def send_data(self, vibration_data, timestamp):
        """ Send vibration data as a csv file
        """
        metadata = {'timestamp': timestamp}
        filename = "vibration_data.csv" % timestamp
        np.savetxt(filename, vibration_data, fmt='%.6f', delimiter=',')

        with open(filename, 'rb') as f:
            r = requests.post(self.url, data=metadata, files={filename: f})
        
        return r.status_code

    def send_prediction(self, prediction, timestamp):
        data = {'timestamp': timestamp, 'prediction': prediction}
        r = requests.post(self.url, data=data)
        
        return r.status_code

    def check_connection(self):
        r = requests.get(self.url)
        return r.status_code

    def close(self):
        raise NotImplementedError()