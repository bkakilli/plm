import time
import numpy as np
from dronekit import connect, VehicleMode

class Drone:
    def __init__(self):
        self.connected = False

    def connect(self, connection_string, listen_period=30):
        """ Connect to drone
        Inputs:
            (str) Device name of which the drone is connected
            (int) How many seconds listen for vibration data
        """

        print("\nConnecting to vehicle on: %s" % connection_string)
        self.vehicle = connect(connection_string, baud=57600, wait_ready=False)

        self.vehicle.wait_ready('autopilot_version')
        self.listen_period = listen_period
        self.connected = True

    def get_data(self):
        
        vib_Data = []

        @self.vehicle.on_message('VIBRATION')
        def my_method(self, name, msg):
            tokens = str(msg).split(" ")
            vib_Data.append([
                float(tokens[3][:-1]) / (10**6),
                float(tokens[6][:-1]),
                float(tokens[9][:-1]),
                float(tokens[12][:-1])])
            
        time.sleep(self.listen_period)

        return np.array(vib_Data)

    def close(self):
        self.vehicle.close()
