
import torch
import torch.utils.data as Data
import torch.nn as nn

class RNN(nn.Module):

    def __init__(self, input_size, num_cases, data_len):
        super(RNN,self).__init__()
        self.data_len = data_len
        self.rnn=nn.LSTM(input_size=input_size,
                        hidden_size=64,
                        num_layers=1,
                        batch_first=True)

        self.out=nn.Linear(64,num_cases)

    def forward(self,x):
        r_out,(h_n, h_c)=self.rnn(x,None)
        out=self.out(r_out[:,-1,:])
        return out