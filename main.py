"""
Product Lifecycle Management
"""
import time
import argparse

from modules.predictor import Predictor
from modules.database import RemoteDatabase
from modules.drone import Drone

class PLM:

    def __init__(self):

        self.predictor = Predictor()
        self.database = RemoteDatabase()
        self.drone = Drone()

    def run(self):
        """ Main routine of PLM program
        """

        while self.alive:

            timestamp = time.time() // 1000
            
            vibration_data = self.drone.get_data()
            predicted_condition = self.predictor.predict(vibration_data)

            self.database.send_data(vibration_data, timestamp)
            self.database.send_prediction(predicted_condition, timestamp)


    def start(self, inet, device):
        """ Starter function

        Starts each submodule and call the main routine.
        """
        
        try:
            # Load LSTM model
            self.predictor.load()
            # Connect to database
            self.database.connect(inet)
            # Connect to drone module
            self.drone.connect(device)

            self.alive = True
            self.run()

        except Exception as e:
            raise e
        finally:
            self.shutdown()

    def shutdown(self):
        """ Safe stoping.
        """
        # Predictor??
        if self.database.connected:
            self.database.close()
        if self.drone.connected:
            self.drone.close()


def main():
    
    # Argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument("INETAddress", type=str,
                        help="INET address of the database in the form IP:Socket. Eg: 123.456.78.90:8080")
    parser.add_argument("-d", "--device", type=str, default="/dev/ttyACM0",
                        help="Device name which the drone is connected to.")
    args = parser.parse_args()

    plm = PLM()
    plm.start(args.inet, args.device)

if __name__ == "__main__":
    main()